import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

// Import Components and pages
import Header from './Components/Header/Header';
import LandingPage from './Components/Pages/LandingPage';
import HomePage from './Components/Pages/HomePage';
import CovidQuestion1 from './Components/Pages/CovidQuestion1';
import CovidQuestion2 from './Components/Pages/CovidQuestion2';
import CovidQuestion3 from './Components/Pages/CovidQuestion3';
import CovidQuestion4 from './Components/Pages/CovidQuestion4';
import CovidEntryRejection from './Components/Pages/CovidEntryRejection';
import Translate from './Components/Pages/Translate';

// CSS sytle sheet
import './style.css';

function App() {
  return (
    <Router>
      <div className="App">

        <Header />

        <Route exact path='/' component={LandingPage} />
        <Route exact path='/selectLanguage' component={HomePage} />
        <Route exact path='/covid19-q1' component={CovidQuestion1} />
        <Route exact path='/covid19-q2' component={CovidQuestion2} />
        <Route exact path='/covid19-q3' component={CovidQuestion3} />
        <Route exact path='/covid19-q4' component={CovidQuestion4} />
        <Route exact path='/covid19-accessDenied' component={CovidEntryRejection} />
        <Route exact path='/translate' component={Translate} />

      </div>
    </Router>
  );
}

export default App;