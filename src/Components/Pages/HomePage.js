import React, { useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import MicrophoneIcon from '../../Images/MicrophoneIcon.png';
import { Helmet } from "react-helmet";


function HomePage() {
  // Array of instruction messages in all languages shown on home page
  const textArray = ['选择或说出您喜欢的语言', 'Επιλέξτε ή πείτε τη γλώσσα που προτιμάτε', 'Seleziona o pronuncia la tua lingua preferita', 'Wählen oder sagen Sie Ihre bevorzugte Sprache', 'Chọn hoặc nói ngôn ngữ ưa thích của bạn'];
  // Set state of ID number of the text array
  const [textIdx, setTextIdx] = useState(0);

  // useEffect function to run whenever the instruction text array changes
  useEffect(() => {
    let timeout;
    // Cycles through the instruction messages in different languages
    if (textIdx < textArray.length - 1) {
      timeout = setTimeout(() => setTextIdx(textIdx + 1), 3000);
    } else {
      timeout = setTimeout(() => setTextIdx(0), 3000);
    }
    return () => {
      clearTimeout(timeout);
    }
  }, [textArray, textIdx]);

  // Variable used to direct to other pages
  const history = useHistory();
  // Function to direct the user to the appropriate page depending on selection on landing page
  function direct() {
    if (localStorage.getItem("officeMode") === "frontOffice") {
      console.log(localStorage.getItem("officeMode"));
      history.push("/covid19-q1");
    } else {
      console.log(localStorage.getItem("officeMode"));
      history.push("/translate")
    }
  }

  return (
    <div>
      <Helmet>
        <title>Language Translation | Select Language</title>
      </Helmet>

      <div>
        <h1>Language Translation</h1>
        <h2>Select or say your preferred language <span><img className="imageBtn-inactive" src={MicrophoneIcon} title="Speech input currently unavailable" alt="Speech input currently unavailable" /></span></h2>
        <h2><i>{textArray[textIdx]}</i></h2>
      </div>
      <div className='languageBtnsGrid'>
        <Link to='/selectLanguage'>
          <button className='languageBtn-disabled' title='Language coming soon'><b>Chinese</b><br /><i className='chineseSymbols'>简体中文</i><br /><br /><br /><i>Coming<br />Soon</i></button>
        </Link>
        <Link to='/selectLanguage'>
          <button className='languageBtn-disabled' title='Language coming soon'><b>Greek</b><br /><i>Ελληνικά<br /><br /><br />Coming<br />Soon</i></button>
        </Link>
        <Link to='/selectLanguage'>
          <button className='languageBtn-disabled' title='Language coming soon'><b>Italian</b><br /><i>Italiano<br /><br /><br />Coming<br />Soon</i></button>
        </Link>
        <button className='gridItems languageBtn-active' onClick={() => (localStorage.setItem("languageSelected", "de"), direct())}><b>German</b><br /><i>Deutsche<br /><br /><br /><br />Hallo</i></button>
        <button className='gridItems languageBtn-active' onClick={() => (localStorage.setItem("languageSelected", "vi"), direct())}><b>Vietnamese</b><br /><i>Tiếng Việt<br /><br /><br /><br />Xin chào</i></button>
      </div>
    </div>
  );
}

export default HomePage;
