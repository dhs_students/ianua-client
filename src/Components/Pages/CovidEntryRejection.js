import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import IdleTimeout from "../IdleTimeout";
import { Helmet } from "react-helmet";

function CovidQuestionEntryRejection() {
  // Retrieve the language selected at the home page from localStorage
  const [langaugeSelected] = useState(localStorage.getItem('languageSelected'));
  // Empty variables for all pre-filled areas of the COVID-19 rejection page that are changed according to the non-English language selected
  const [pageInstructions, setPageInstructions] = useState("");
  const [covidInstruction, setCovidInstruction] = useState("");
  const [okText, setOkText] = useState("");

  // Function that runs when the page is loaded
  useEffect(() => {
    // Switch statement to assign the pre-filled text with the relevant language chosen at the beginning 
    switch (langaugeSelected) {
      // Vietnamese variables 
      case "vi":
        {
          setPageInstructions("Quyền truy cập bị từ chối");
          setCovidInstruction("Vui lòng rời khỏi Trung Tâm Dịch Vụ và hoàn tất công việc trên mạng hoặc qua điện thoại.");
          setOkText("Đồng ý");
          break;
        }
      // German variables
      case "de":
        {
          setPageInstructions("Zugriff verweigert");
          setCovidInstruction("Bitte verlassen Sie das Service Center und erledigen Sie alle Geschäfte online oder telefonisch.");
          setOkText("OK");
          break;
        }
      default:
        console.log("Error: No language selected");
    }
  }, [langaugeSelected]);

  return (
    <div className="body">
      <Helmet>
        <title>Language Translation | No Entry</title>
      </Helmet>

      <IdleTimeout />

      <h1>COVID-19</h1>
      <h2><i>{pageInstructions}</i><br /><b>Access denied</b></h2>
      <div className='covidQuestion-nonEng'><i>{covidInstruction}</i></div>
      <div className='covidQuestion-eng'><b>Please leave the Service Centre and complete any business online or by phone.</b></div>
      <div className='okBtnContainer'>
        <Link to='/selectLanguage'>
          <button className='okBtn'><i>{okText}</i><br /><b>OK</b></button>
        </Link>
      </div>
    </div>
  );
}

export default CovidQuestionEntryRejection;