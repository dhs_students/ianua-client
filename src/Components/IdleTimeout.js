import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import IdleTimer from 'react-idle-timer'

// Style sheet for popup Modal used
import 'bootstrap/dist/css/bootstrap.min.css';

function IdleTimout() {
  // Retrieve the language selected at the home page from localStorage
  const [langaugeSelected] = useState(localStorage.getItem('languageSelected'));
  // Empty variables for all pre-filled areas of the COVID-19 Question 1 page that are changed according to the non-English language selected
  const [popupTitle, setPopupTitle] = useState("");
  const [popupBody, setPopupBody] = useState("");
  const [continueBtn, setContinueBtn] = useState("");
  const [endBtn, setEndBtn] = useState("");

  // Function that runs when a page is loaded that contains the modal component
  useEffect(() => {
    // Switch statement to assign the pre-filled text with the relevant language chosen at the beginning
    switch (langaugeSelected) {
      // Vietnamese variables 
      case "vi":
        {
          setPopupTitle("Cảnh báo thời gian chờ của phiên");
          setPopupBody("Phiên của bạn sẽ hết hạn sau 5 phút");
          setContinueBtn("Tiếp tục");
          setEndBtn("Kết thúc");
          break;
        }
      // German variables
      case "de":
        {
          setPopupTitle("Warnung zum Sitzungszeitlimit");
          setPopupBody("Ihre Sitzung läuft in 5 Minuten ab");
          setContinueBtn("Fortsetzen");
          setEndBtn("Ende");
          break;
        }
      default:
        console.log("Error: No language selected");
    }
  }, [langaugeSelected]);

  // Object to hold history and allow redirecting to other pages
  const history = useHistory();
  // Set a show state for the Modal that appears when user is inactive for a period of time
  const [show, setShow] = useState(false);
  // Create the toggle functions for when the Modal appears/disappears
  function handleClose() {
    setShow(false)
  }
  function handleShow() {
    setShow(true);
  }

  // Function that directs user to the home page
  function returnHome() {
    history.push("/selectLanguage")
  }

  // Variable that holds the details of the idle timer
  var idleTimer = null
  // Function actioned whenever there is prolonged inactivity
  function handleOnIdle() {
    // Check if popup Modal is shown
    if (show) {
      // Return to home page
      console.log("User still idle - Terminating session");
      returnHome();
    } else {
      // Show popup Modal box
      handleShow();
      console.log("User has been idle for 5min");
    }
    // Reset the timer so the idle timer can be triggered again after the popup appears
    idleTimer.reset();
  }

  return (
    <div>
      <IdleTimer
        ref={ref => { idleTimer = ref }}
        timeout={1000 * 60 * 5}
        onIdle={handleOnIdle}
        debounce={250}
      />

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title><i>{popupTitle}</i><br />Session timeout warning</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <i>{popupBody}</i><br />Your session will expire in 5 minutes
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="primary"
            onClick={handleClose}
          >
            <i>{continueBtn}</i> <b>/ Continue</b>
          </Button>
          <Button
            variant="secondary"
            onClick={returnHome}
          >
            <i>{endBtn}</i> <b>/ End</b>
            </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default IdleTimout;